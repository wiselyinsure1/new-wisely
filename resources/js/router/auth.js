import Auth from "../views/auth/index";
import Login from "../views/auth/login";
import Register from "../views/auth/register";

export default
{
    path: '/auth',
    component: Auth,
    children: [
        { path: 'login', component: Login},
        { path: 'register', component: Register}
    ]

}
