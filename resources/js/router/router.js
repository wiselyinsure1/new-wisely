import vue from "vue";
import VueRouter from "vue-router";
import AuthRouter from "./auth";
import LandingRouter from "./landing";
import AdminRouter from "./admin";
// import login from "./views/login";
// import ExampleComponent from "./components/ExampleComponent"
// import help from "../views/help.vue";
import main from "../views/main";
import ClientApp from "../views/client/index.vue";
// import ClientAuth from "./views/auth/client"



vue.use(VueRouter);


const routes =[
    {
        path:"/",
        name:"main",
        component: main,
        children:[
            AuthRouter,
            LandingRouter,
            AdminRouter
        ]
    },
    {
        path:"/app",
        name:"app",
        component: ClientApp
    }
]

export default new VueRouter({

    routes
});
