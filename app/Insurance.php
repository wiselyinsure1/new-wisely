<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Insurance extends Model
{
    protected $fillable = [
        'name', 'website', 'hotline', 'address', 'agent_name', 'agent_email'
    ];

    public function benefits() {
        return $this->belongsToMany(Benefit::class);
    }
}
