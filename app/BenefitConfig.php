<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BenefitConfig extends Model
{
    protected $fillable = ['benefit_id', 'label', 'type', 'suffix'];

    public function children() {
        return $this->hasMany(BenefitConfig::class);
    }
}
