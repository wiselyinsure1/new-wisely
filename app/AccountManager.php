<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class AccountManager extends Authenticatable implements JWTSubject
{
    use Notifiable;

    protected $guard = 'account-manager';

    protected $fillable = [
        'name', 'email', 'password', 'mobile'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function setPasswordAttribute($value){
        $this->attributes['password'] = bcrypt($value);
    }

    public function clients() {
        return $this->hasMany(AccountManager::class);
    }
}
