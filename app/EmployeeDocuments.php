<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeDocuments extends Model
{
    protected $fillable = [
        'key', 'name', 'file', 'expiry_date'
    ];

    protected $appends = [
        'file_link'
    ];

    public function getFileLinkAttribute() {
        if(!$this->file) {
            return null;
        }

        return env('APP_URL') . '/' . $this->file;
    }
}
