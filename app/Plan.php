<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $fillable = [
        'title', 'insurance_id', 'is_custom', 'benefit_id'
    ];

    protected $casts = [
        'is_custom' => 'boolean',
    ];

    public function details() {
        return $this->hasMany(PlanDetails::class);
    }

    public function insurance() {
        return $this->belongsTo(Insurance::class);
    }

    public function client() {
        return $this->belongsTo(Client::class);
    }

    public function benefit() {
        return $this->belongsTo(Benefit::class);
    }
}
