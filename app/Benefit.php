<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Benefit extends Model
{
    protected $fillable = ['label'];

    public function configs() {
        return $this->hasMany(BenefitConfig::class);
    }
}
