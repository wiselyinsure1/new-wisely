<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanDetails extends Model
{
    protected $fillable = [
        'benefit_config_id', 'value'
    ];
}
