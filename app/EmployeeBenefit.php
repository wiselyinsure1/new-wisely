<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeBenefit extends Model
{
    protected $fillable = [
        'benefit_id', 'insurance_id', 'plan_id', 'addition_date'
    ];

    protected $with = [
        'benefit',
        'insurance',
        'plan'
    ];

    public function benefit() {
        return $this->belongsTo(Benefit::class);
    }

    public function insurance() {
        return $this->belongsTo(Insurance::class);
    }

    public function plan() {
        return $this->belongsTo(Plan::class);
    }
}
