<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = [
        'name', 'no_of_employees', 'is_holding', 'approved_at', 'declined_at'
    ];

    protected $casts = [
        'is_holding' => 'boolean',
        'approved_at' => 'datetime',
        'declined_at' => 'datetime',
    ];

    protected $appends = [
        'status'
    ];

    public function users() {
        return $this->hasMany(User::class);
    }

    public function user() {
        return $this->hasOne(User::class);
    }

    public function accountManager() {
        return $this->belongsTo(AccountManager::class);
    }

    public function employees() {
        return $this->hasMany(Employee::class);
    }

    public function getStatusAttribute() {
        if (!$this->approved_at) {
            return 'new';
        }

        if ($this->approved_at && !$this->declined_at) {
            return 'approved';
        }

        if ($this->declined_at) {
            return 'declined';
        }

        return 'unknown';
    }
}
