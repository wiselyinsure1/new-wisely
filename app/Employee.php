<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = [
        'first_name', 'middle_name', 'last_name', 'mobile', 'email', 'employee_id', 'birthdate', 'marital_status', 'gender', 'job_title', 'department', 'hiring_date', 'contract_type', 'contract_expire', 'salary', 'image',
    ];

    public function benefits() {
        return $this->hasMany(EmployeeBenefit::class);
    }

    public function family() {
        return $this->hasMany(EmployeeFamily::class);
    }

    public function documents() {
        return $this->hasMany(EmployeeDocuments::class);
    }
}
