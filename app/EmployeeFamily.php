<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeFamily extends Model
{
    protected $fillable = [
        'name', 'relationship', 'birthdate', 'mobile', 'address'
    ];

    protected $appends = [
        'image_link'
    ];

    public function benefits() {
        return $this->hasMany(EmployeeFamilyBenefit::class);
    }

    public function getImageLinkAttribute() {
        if(!$this->image) {
            return null;
        }

        return env('APP_URL') . '/' . $this->image;
    }
}
