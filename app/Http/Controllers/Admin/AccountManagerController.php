<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\AccountManager;

class AccountManagerController extends Controller
{
    public function index(Request $request) {
        $accountManagers = new AccountManager();

        if($request->sortBy) {
            $accountManagers = $accountManagers->orderBy($request->sortBy, $request->sortDir === 'DESC' ? 'DESC' : 'ASC');
        }
        if(!empty($request->q)) {
            $accountManagers = $accountManagers->where('name', 'LIKE', '%' . $request->q . '%')
                        ->orWhere('email', 'LIKE', '%' . $request->q . '%')
                        ->orWhere('mobile', 'LIKE', '%' . $request->q . '%');
        }

        $accountManagers = $accountManagers->paginate($request->size);

        return response()->json($accountManagers);
    }

    public function show(AccountManager $accountManager) {
        return response()->json($accountManager);
    }

    public function store(\App\Http\Requests\Admin\AccountManagerRequest $request) {
        $accountManager = AccountManager::create($request->all());
        return response()->json($accountManager);
    }

    public function update(AccountManager $accountManager, \App\Http\Requests\Admin\AccountManagerRequest $request) {
        $accountManager->update($request->except(['password']));
        $accountManager->save();
        return response()->json($accountManager);
    }

    public function updatePassword(AccountManager $accountManager, \App\Http\Requests\Admin\AccountManagerPasswordRequest $request) {
        $accountManager->password = $request->get('password');
        $accountManager->save();
        return response()->json($accountManager);
    }

    public function destroy(AccountManager $accountManager) {
        $accountManager->delete();
        return response()->json('Item deleted');
    }
}
