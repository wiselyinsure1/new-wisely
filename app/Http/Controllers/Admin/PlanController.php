<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Plan;
use App\PlanDetails;
use App\Insurance;
use App\Client;

class PlanController extends Controller
{
    public function index(Request $request) {
        $plans = Plan::with('insurance', 'client', 'benefit');

        if($request->sortBy) {
            $plans = $plans->orderBy($request->sortBy, $request->sortDir === 'DESC' ? 'DESC' : 'ASC');
        }
        if(!empty($request->client_id)) {
            $plans = $plans->where('client_id', $request->client_id);
        }

        $plans = $plans->paginate($request->size);

        return response()->json($plans);
    }

    public function show(Plan $plan) {
        $plan->load(['client', 'insurance', 'details']);
        return response()->json($plan);
    }

    public function store(Request $request) {
        $plan = new Plan([
            'title' => $request->title,
            'benefit_id' => $request->benefit_id,
        ]);
        if ($request->client_id) {
            $plan->client()->associate( Client::findOrFail($request->client_id) );
        }
        $plan->insurance()->associate( Insurance::findOrFail($request->insurance_id) );
        $plan->save();

        $details = collect($request->details)->map(function($value, $benefit_config_id) {
            return new PlanDetails(['benefit_config_id' => $benefit_config_id, 'value' => $value]);
        })->toArray();

        $plan->details()->createMany($details);

        $plan->load(['details', 'benefit']);

        return response()->json($plan);
    }

    public function destroy(Plan $plan) {
        $plan->delete();

        return "Plan deleted!";
    }
}
