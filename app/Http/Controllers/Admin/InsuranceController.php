<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Insurance;

class InsuranceController extends Controller
{
    public function index(Request $request) {
        $insurances = Insurance::with('benefits');

        if($request->sortBy) {
            $insurances = $insurances->orderBy($request->sortBy, $request->sortDir === 'DESC' ? 'DESC' : 'ASC');
        }
        if(!empty($request->q)) {
            $insurances = $insurances->where('name', 'LIKE', '%' . $request->q . '%');
        }
        if(!empty($request->benefits)) {
            $benefitIds = explode(',', $request->benefits);
            $insurances = $insurances->whereHas('benefits', function($q) use ($benefitIds) {
                $q->whereIn('benefits.id', $benefitIds);
            });
        }

        $insurances = $insurances->paginate($request->size);

        return response()->json($insurances);
    }

    public function show(Insurance $insurance) {
        $insurance->load(['benefits']);
        return response()->json($insurance);
    }

    public function store(\App\Http\Requests\Admin\InsuranceRequest $request) {
        $insurance = Insurance::create($request->all());
        $insurance->benefits()->attach($request->benefits);
        $insurance->load('benefits');
        return response()->json($insurance);
    }
}
