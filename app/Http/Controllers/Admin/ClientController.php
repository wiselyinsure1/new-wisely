<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Client;
use App\AccountManager;

class ClientController extends Controller
{
    public function index(Request $request) {
        $clients = Client::with('users', 'accountManager');

        if($request->sortBy) {
            $clients = $clients->orderBy($request->sortBy, $request->sortDir === 'DESC' ? 'DESC' : 'ASC');
        }
        if(!empty($request->q)) {
            $clients = $clients->where('name', 'LIKE', '%' . $request->q . '%');
        }

        switch($request->status) {
            case 'new':
                $clients = $clients->whereNull('approved_at');
            break;

            case 'active':
                $clients = $clients->whereNotNull('approved_at')->whereNull('declined_at');
            break;

            case 'declined':
                $clients = $clients->whereNotNull('declined_at');
            break;
        }

        $clients = $clients->paginate($request->size);

        return response()->json($clients);
    }

    public function show(Client $client) {
        $client->load(['users', 'accountManager']);
        return response()->json($client);
    }

    public function approve(Client $client) {
        $client->approved_at = now();
        $client->save();
        return response()->json('Client request approved');
    }

    public function decline(Client $client) {
        $client->declined_at = now();
        $client->save();
        return response()->json('Client request declined');
    }

    public function accountManager(Client $client, Request $request) {
        $client->accountManager()->associate( AccountManager::findOrFail($request->get('account_manager_id')) );
        $client->save();

        return 'Account manager assgined to client';
    }
}
