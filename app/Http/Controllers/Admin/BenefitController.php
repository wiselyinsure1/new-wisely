<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Benefit;

class BenefitController extends Controller
{
    public function index() {
        $benefits = Benefit::get();

        return response()->json($benefits);
    }

    public function show(Benefit $benefit) {
        $benefit->load(['configs' => function($q) {
            $q->whereNull('benefit_config_id');
            $q->with('children');
        }]);

        return response()->json($benefit);
    }

    public function store(Request $request) {
        $benefit = Benefit::create($request->all());

        return response()->json($benefit);
    }

    public function addConfig(Benefit $benefit, Request $request) {
        $config = $benefit->configs();
        $data = $request->all();

        if ($request->parent_id) {
            $config = $config->find($request->parent_id)->children();
            $data['benefit_id'] = $benefit->id;
        }

        $config = $config->create($data);

        return response()->json($config);
    }

    public function updateConfig(Benefit $benefit, $config_id, Request $request) {
        $benefit->configs()->where('id', $config_id)->update($request->all());
        $config = $benefit->configs()->find($config_id);

        return response()->json($config);
    }

    public function deleteConfig(Benefit $benefit, $config_id) {
        $config = $benefit->configs()->find($config_id);
        $config->delete();

        return response()->json('Configuration deleted');
    }
}
