<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Admin;

class AdminController extends Controller
{
    public function index(Request $request) {
        $admins = new Admin();

        if($request->sortBy) {
            $admins = $admins->orderBy($request->sortBy, $request->sortDir === 'DESC' ? 'DESC' : 'ASC');
        }
        if(!empty($request->q)) {
            $admins = $admins->where('name', 'LIKE', '%' . $request->q . '%')
                        ->orWhere('email', 'LIKE', '%' . $request->q . '%')
                        ->orWhere('mobile', 'LIKE', '%' . $request->q . '%');
        }

        $admins = $admins->paginate($request->size);

        return response()->json($admins);
    }

    public function show(Admin $admin) {
        return response()->json($admin);
    }

    public function store(\App\Http\Requests\Admin\AdminRequest $request) {
        $admin = Admin::create($request->all());
        return response()->json($admin);
    }

    public function update(Admin $admin, \App\Http\Requests\Admin\AdminRequest $request) {
        $admin->update($request->except(['password']));
        $admin->save();
        return response()->json($admin);
    }

    public function updatePassword(Admin $admin, \App\Http\Requests\Admin\AdminPasswordRequest $request) {
        $admin->password = $request->get('password');
        $admin->save();
        return response()->json($admin);
    }

    public function destroy(Admin $admin) {
        $admin->delete();
        return response()->json('Item deleted');
    }
}
