<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Benefit;

class BenefitController extends Controller
{
    public function index() {
        $benefits = Benefit::get();

        return $benefits;
    }
}
