<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Image;
use App\Employee;
use Illuminate\Support\Str;

class EmployeeController extends Controller
{
    public function index(Request $request) {
        $employees = new Employee();

        if($request->sortBy) {
            $employees = $employees->orderBy($request->sortBy, $request->sortDir === 'DESC' ? 'DESC' : 'ASC');
        }
        if(!empty($request->q)) {
            $employees = $employees->where('first_name', 'LIKE', '%' . $request->q . '%')
                        ->orWhere('middle_name', 'LIKE', '%' . $request->q . '%')
                        ->orWhere('last_name', 'LIKE', '%' . $request->q . '%')
                        ->orWhere('mobile', 'LIKE', '%' . $request->q . '%')
                        ->orWhere('email', 'LIKE', '%' . $request->q . '%')
                        ->orWhere('employee_id', 'LIKE', '%' . $request->q . '%');
        }

        $employees = $employees->paginate($request->size);

        return response()->json($employees);
    }

    public function show(Employee $employee) {
        return response()->json($employee);
    }

    public function store(Request $request) {
        $client = auth()->user()->client;

        $employee = $client->employees()->create($request->all());

        return response()->json($employee);
    }

    public function update(Employee $employee, Request $request) {
        $employee->update($request->all());

        return response()->json($employee);
    }

    public function benefits(Employee $employee) {
        return response()->json($employee->benefits);
    }

    public function addBenefit(Employee $employee, Request $request) {
        $benefit = $employee->benefits()->create($request->all());
        $benefit->load(['benefit', 'insurance', 'plan']);

        return response()->json($benefit);
    }

    public function deleteBenefit(Employee $employee, $benefit) {
        $employee->benefits()->find($benefit)->delete();

        return response()->json('Employee benefit deleted');
    }

    public function family(Employee $employee) {
        $family = $employee->family;
        $family->load(['benefits']);

        return response()->json($family);
    }

    public function addFamily(Employee $employee, Request $request) {
        $family = $employee->family()->create($request->all());

        return response()->json($family);
    }

    public function updateFamily(Employee $employee, $family_id, Request $request) {
        $employee->family()->where('id', $family_id)->update($request->all());
        $family = $employee->family()->find($family_id);

        return response()->json($family);
    }

    public function deleteFamily(Employee $employee, $family_id) {
        $family = $employee->family()->find($family_id);
        $image = $family->image;
        $family->delete();

        Storage::delete('public/' . $image);

        return response()->json('Family member deleted');
    }

    public function addFamilyBenefit(Employee $employee, $family_id, Request $request) {
        $family = $employee->family()->find($family_id);
        $benefit = $family->benefits()->create($request->all());
        $benefit->load(['benefit', 'insurance', 'plan']);

        return response()->json($benefit);
    }

    public function deleteFamilyBenefit(Employee $employee, $family_id, $benefit_id) {
        $family = $employee->family()->find($family_id);
        $family->benefits()->find($benefit_id)->delete();

        return response()->json('Benefit deleted');
    }

    public function updateFamilyImage(Employee $employee, Request $request, $family_id) {
        if(!$request->hasFile('image')) {
            return response()->json('Image is required', 403);
        }

        $base = 'employee/' . $employee->id;
        $filename = $base . '/f_' . time() . '.jpg';
        $file = $request->file('image');
        $image = Image::make($file)->encode('jpg');

        $saved = Storage::put('public/' . $filename, $image);

        if(!$saved) {
            return response()->json('Could not save file', 500);
        }
        $family = $employee->family()->find($family_id);
        $oldImg = $family->image;
        $family->image = $filename;
        $family->save();

        Storage::delete('public/' . $oldImg);

        return response()->json( $family->image_link );
    }

    public function documents(Employee $employee) {
        $documents = $employee->documents;

        return response()->json($documents);
    }

    public function saveDocument(Employee $employee, Request $request, $document_id = null) {
        $file = $request->file('file');
        if ($file) {
            $base = 'employee/' . $employee->id;
            $filename = $base . '/d_' . time();
            $extention = '.' .$file->extension();
            $filenameWExt = $filename . $extention;

            $path = $file->storeAs('public/', $filenameWExt);

            if(!$path) {
                return response()->json('Could not save file', 500);
            }
            $MAX_LENGTH = 255;
            $name = $file->getClientOriginalName();
            if (Str::length($name) > $MAX_LENGTH) {
                $name = Str::limit($name, $MAX_LENGTH - Str::length($extention)) . '.'. $extention;
            }
        }

        $documents = $employee->documents();

        if ($document_id) {
            $document = $documents->find($document_id);
            if ($file) {
                $oldFile = $document->file;
                $document->file = $path;
                $document->name = $name;
            }
            $document->expiry_date = $request->expiry_date;
            $document->save();
            if($file) {
                Storage::delete('public/' . $oldFile);
            }
        } else {
            $document = $documents->create([
                'file' => $filename,
                'name' => $name,
                'expiry_date' => $request->expiry_date,
                'key' => $request->key
            ]);
        }

        return response()->json($document);
    }
}
