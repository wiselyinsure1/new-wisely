<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Client;

class AuthController extends Controller
{
    public function login(Request $request) {
        $credentials = $request->only(['email', 'password']);

        if (!$token = auth('client')->attempt($credentials)) {
            return response()->json(['error' => 'Invalid credentials'], 401);
        }

        return $this->respondWithToken($token);
    }

    public function register(\App\Http\Requests\Client\RegisterRequest $request) {
        $client = Client::create([
            'name' => $request->client_name,
            'no_of_employees' => $request->no_of_employees,
            'is_holding' => $request->is_holding or false,
        ]);

        $user = $client->users()->create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
            'mobile' => $request->mobile
        ]);

        if (!$token = auth('client')->login($user)) {
            return response()->json(['error' => 'Could not create token'], 500);
        }

        return $this->respondWithToken($token);
    }

    private function respondWithToken($token) {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('client')->factory()->getTTL() * 60,
        ]);
    }
}
