<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Plan;

class PlanController extends Controller
{
    public function index(Request $request) {
        $client_id = auth()->user()->client_id;
        $plans = Plan::where('client_id', $client_id)
                ->when($request->has('insurance_id'), function($q) use ($request) {
                    $q->where('insurance_id', $request->insurance_id);
                })
                ->get();

        return $plans;
    }
}
