<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function() {
    Route::post('login', 'AuthController@login');

    Route::group(['middleware' => 'auth:admin'], function() {
        Route::get('profile', 'ProfileController@index');
        Route::apiResource('admin', 'AdminController');
        Route::put('admin/{admin}/password', 'AdminController@updatePassword');
        Route::apiResource('account-manager', 'AccountManagerController');
        Route::apiResource('benefit', 'BenefitController');
        Route::post('benefit/{benefit}/config', 'BenefitController@addConfig');
        Route::put('benefit/{benefit}/config/{config}', 'BenefitController@updateConfig');
        Route::delete('benefit/{benefit}/config/{config}', 'BenefitController@deleteConfig');
        Route::apiResource('insurance', 'InsuranceController');
        Route::apiResource('client', 'ClientController');
        Route::put('client/{client}/approve', 'ClientController@approve');
        Route::put('client/{client}/decline', 'ClientController@decline');
        Route::put('client/{client}/account-manager', 'ClientController@accountManager');
        Route::apiResource('plan', 'PlanController');
    });
});

Route::group(['prefix' => 'client', 'namespace' => 'Client'], function() {
    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');

    Route::group(['middleware' => 'auth:client'], function() {
        Route::get('profile', 'ProfileController@index');
        Route::get('benefit', 'BenefitController@index');
        Route::get('insurance', 'InsuranceController@index');
        Route::get('plan', 'PlanController@index');
        Route::apiResource('employee', 'EmployeeController');
        Route::group(['prefix' => 'employee/{employee}'], function() {
            Route::get('benefit', 'EmployeeController@benefits');
            Route::post('benefit', 'EmployeeController@addBenefit');
            Route::delete('benefit/{benefit}', 'EmployeeController@deleteBenefit');
            Route::get('family', 'EmployeeController@family');
            Route::post('family', 'EmployeeController@addFamily');
            Route::put('family/{family}', 'EmployeeController@updateFamily');
            Route::delete('family/{family}', 'EmployeeController@deleteFamily');
            Route::post('family/{family}/benefit', 'EmployeeController@addFamilyBenefit');
            Route::delete('family/{family}/benefit/{benefit}', 'EmployeeController@deleteFamilyBenefit');
            Route::patch('family/{family}/image', 'EmployeeController@updateFamilyImage');
            Route::get('document', 'EmployeeController@documents');
            Route::post('document/{document?}', 'EmployeeController@saveDocument');
        });
    });
});
