<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBenefitConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('benefit_configs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('benefit_id')->unsigned();
            $table->bigInteger('benefit_config_id')->unsigned()->nullable();
            $table->string('label');
            $table->string('type');
            $table->string('suffix')->nullable();
            $table->timestamps();

            $table->foreign('benefit_id')->references('id')->on('benefits')->onDelete('cascade');
            $table->foreign('benefit_config_id')->references('id')->on('benefit_configs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('benefit_configs');
    }
}
