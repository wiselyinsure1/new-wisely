<?php

use Illuminate\Database\Seeder;
use App\Admin;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::create([
            'name' => 'System Admin',
            'email' => '1@1.com',
            'password' => '123123',
            'is_super' => true,
            'mobile' => '01000069169'
        ]);
    }
}
