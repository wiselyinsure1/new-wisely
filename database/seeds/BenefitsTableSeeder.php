<?php

use Illuminate\Database\Seeder;
use App\Benefit;

class BenefitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Benefit::create([
            'label' => 'Medical'
        ]);

        Benefit::create([
            'label' => 'Life'
        ]);

        Benefit::create([
            'label' => 'Accident'
        ]);

        Benefit::create([
            'label' => 'Life & Medical'
        ]);
    }
}
